module document-collector

go 1.16

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/sirupsen/logrus v1.8.1
	github.com/streadway/amqp v1.0.0
	go.mongodb.org/mongo-driver v1.5.3
	go.uber.org/dig v1.11.0
	gopkg.in/yaml.v2 v2.4.0
)
