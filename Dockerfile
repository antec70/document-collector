FROM golang:1.16

ADD . /src/document-collector

WORKDIR /src/document-collector

RUN go mod download

RUN go install ./app/cmd

ENTRYPOINT /go/bin/cmd
