# Document-collector
## Стек:
- Go 1.16
- RabbitMQ
- MongoDb

Микросервис является частью системы управления контентом.
Задачей микросервиса является поддержание актуального состояния контента в MongoDB.

Получая сообщение из очереди RabbitMQ сервис загружает идентификаторы записей в количестве `limit`, появившиеся в  MySQL в период с последнего запуска сервиса и отправляет сообщения в RabbitMQ с этими идентификаторами, которые обрабатывает другой сервис импорта в MongoDB.


### Запуск:
Для запуска необходимо указать путь до конфига формата .yaml
~~~
docker run -it -v $(pwd)/path/to-config/params-local.yaml:/src/document-collector/params-local.yaml <imageId>
~~~
Пример конфига
```yaml
debug: true
rabbitMQ:
  host: localhost
  port: 5672
  user: "guest"
  password: "guest"
  routing_key: "documentCollector"
  doc_importer: "documentImporter"

mongo: "mongodb://localhost:27017/"
mysql:
  host: "localhost"
  port: 3306
  user: "user"
  password: "user"
  db: "test"

limit: 5

```