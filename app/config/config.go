package config

import (
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
)

type ParamsLocal struct {
	Debug     bool `yaml:"debug"`
	Limit     int  `yaml:"limit"`
	RabbitCon struct {
		Host        string `yaml:"host"`
		Port        int    `yaml:"port"`
		User        string `yaml:"user"`
		Password    string `yaml:"password"`
		RoutingKey  string `yaml:"routing_key"`
		DocImporter string `yaml:"doc_importer"`
	} `yaml:"rabbitMQ"`
	MysqlCon struct {
		Host     string `yaml:"host"`
		Port     int    `yaml:"port"`
		User     string `yaml:"user"`
		Password string `yaml:"password"`
		Db       string `yaml:"db"`
	} `yaml:"mysql"`
	MongoDb struct {
		Host       string `yaml:"host"`
		Port       int    `yaml:"port"`
		User       string `yaml:"user"`
		Db         string `yaml:"db"`
		Collection string `yaml:"collection"`
		Password   string `yaml:"password"`
	} `yaml:"mongoDB"`
}

func NewConfig(url string) ParamsLocal {
	var c ParamsLocal
	dat, err := ioutil.ReadFile(url)
	if err != nil {
		log.Fatal(err)
	}
	er := yaml.Unmarshal(dat, &c)
	if er != nil {
		log.Fatalf("error: %v", er)
	}
	if c.Debug == true {
		logrus.Info("Debug mode activated")
	}
	return c
}
