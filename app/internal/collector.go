package internal

import (
	"document-collector/app/config"
	"document-collector/app/service"
	"encoding/json"
	"github.com/sirupsen/logrus"
	"strconv"
)

type Collector struct {
	config        config.ParamsLocal
	mongoService  service.MongoService
	mysqlService  service.MysqlService
	rabbitService service.RabbitService
	logger        *logrus.Logger
	WakeUp        chan []byte
}

func NewCollector(conf config.ParamsLocal, mongoSer service.MongoService, mysqlSer service.MysqlService, rabbitServ service.RabbitService) *Collector {
	return &Collector{
		config:        conf,
		mongoService:  mongoSer,
		mysqlService:  mysqlSer,
		rabbitService: rabbitServ,
		logger:        logrus.New(),
		WakeUp:        make(chan []byte),
	}
}

func (c *Collector) worker() {

	date, err := c.mongoService.GetImporterSystemTime()
	if err != nil {
		c.logger.Fatal(err)
	}

	ro, er := c.mysqlService.GetAllFeed(date)
	if er != nil {
		c.logger.Fatal(er)
	}

	for _, r := range ro {
		if c.config.Debug == true {
			logrus.Info("Get " + r.Type + " Id: " + strconv.Itoa(r.Id))
		}
		jm, _ := json.Marshal(r)
		er := c.rabbitService.Producer(jm)
		if er != nil {
			c.logger.Fatal(er)
		}

	}

}

func (c *Collector) Start() {
	go c.rabbitService.Consumer(c.WakeUp)
	for w := range c.WakeUp {
		logrus.Info(string(w))
		c.worker()
	}

}

func (c *Collector) Shutdown() error {
	close(c.WakeUp)

	err := c.rabbitService.Shutdown()
	if err != nil {
		return err
	}

	er := c.mysqlService.Shutdown()
	if er != nil {
		return er
	}

	e := c.mongoService.Shutdown()

	if e != nil {
		return e
	}
	return nil
}
