package service

import "document-collector/app/repository"

type RabbitService struct {
	rabbit repository.DocRabbitInterface
}

func NewRabbitService(rabbitInterface repository.DocRabbitInterface) RabbitService {
	return RabbitService{rabbit: rabbitInterface}
}

func (rs *RabbitService) Consumer(wakeup chan []byte) {
	rs.rabbit.Consumer(wakeup)
}

func (rs *RabbitService) Producer(m []byte) error {
	return rs.rabbit.Producer(m)
}
func (rs *RabbitService) Shutdown() error {
	return rs.rabbit.Shutdown()
}
