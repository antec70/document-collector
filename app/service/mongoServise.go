package service

import "document-collector/app/repository"

type MongoService struct {
	mongo repository.DocMongoInterface
}

func NewMongoService(mongoInterface repository.DocMongoInterface) MongoService {
	return MongoService{mongo: mongoInterface}
}

func (ms *MongoService) Shutdown() error {
	return ms.mongo.Shutdown()
}

func (ms *MongoService) GetImporterSystemTime() (string, error) {
	return ms.mongo.GetImporterSystemTime()
}
