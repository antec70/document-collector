package service

import (
	"document-collector/app/models"
	"document-collector/app/repository"
)

type MysqlService struct {
	mysql repository.DocMysqlInterface
}

func NewMysqlService(mysqlInterface repository.DocMysqlInterface) MysqlService {
	return MysqlService{mysql: mysqlInterface}
}

func (ms *MysqlService) Shutdown() error {
	return ms.mysql.Shutdown()
}

func (ms *MysqlService) GetAllFeed(date string) ([]models.RabbitOutput, error) {

	docTypes := [3]string{
		"video",
		"news_post",
		"post",
	}
	var ro []models.RabbitOutput
	var sql string
	for _, v := range docTypes {
		switch v {
		case "video":
			sql = `SELECT id from yarus.video
			WHERE yarus.video.create_date < ?
			ORDER BY yarus.video.create_date
			DESC LIMIT ?;`
		case "news_post":
			sql = `SELECT id from yarus.news_post
			WHERE yarus.news_post.original_date < ?
			ORDER BY yarus.news_post.original_date
			DESC LIMIT ?;`
		case "post":
			sql = `SELECT id from yarus.post
			WHERE yarus.post.create_date < ?
			ORDER BY yarus.post.create_date
			DESC LIMIT ?;`

		}
		ids, er := ms.mysql.GetAllFeed(sql, date)

		if er != nil {
			return nil, er

		}

		for _, id := range ids {
			r := models.RabbitOutput{}
			r.Id = id
			r.Type = v
			ro = append(ro, r)
		}

	}

	return ro, nil
}
