package main

import (
	"document-collector/app/config"
	"document-collector/app/internal"
	"document-collector/app/repository/mongo"
	"document-collector/app/repository/mysql"
	"document-collector/app/repository/rabbit"
	"document-collector/app/service"
	"go.uber.org/dig"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

func BuildContainer() *dig.Container {
	container := dig.New()
	container.Provide(func() config.ParamsLocal {
		return config.NewConfig("params-local.yaml")
	})
	container.Provide(mysql.NewDocMysqlRepository)
	container.Provide(rabbit.NewDocRabbitRepository)
	container.Provide(mongo.NewDocMongoRepository)

	container.Provide(service.NewMysqlService)
	container.Provide(service.NewRabbitService)
	container.Provide(service.NewMongoService)
	container.Provide(func(config config.ParamsLocal, mongoService service.MongoService, mysqlService service.MysqlService, rabbitServ service.RabbitService) *internal.Collector {
		return internal.NewCollector(config, mongoService, mysqlService, rabbitServ)
	})
	return container
}

func main() {
	container := BuildContainer()

	err := container.Invoke(func(collector *internal.Collector) {
		wg := sync.WaitGroup{}
		wg.Add(1)

		go func() {
			sigc := make(chan os.Signal)
			signal.Notify(sigc, syscall.SIGINT, syscall.SIGTERM)
			<-sigc

			err := collector.Shutdown()
			if err != nil {
				log.Println(err)
			}
			wg.Done()
		}()

		collector.Start()

		wg.Wait()
	})

	if err != nil {
		log.Fatal(err)
	}
}
