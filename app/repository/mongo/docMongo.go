package mongo

import (
	"context"
	"document-collector/app/config"
	"document-collector/app/models"
	"document-collector/app/repository"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"strconv"
	"time"
)

type DocMongoRepository struct {
	config config.ParamsLocal
	mongo  *mongo.Client
}

func (d DocMongoRepository) GetImporterSystemTime() (string, error) {
	collection := d.mongo.Database(d.config.MongoDb.Db).Collection(d.config.MongoDb.Collection)
	var s models.System
	res := collection.FindOne(context.TODO(), bson.M{"_id": "documentImporter"})
	ch := res.Err()
	if ch != nil {
		if ch.Error() == "mongo: no documents in result" {
			return time.Now().UTC().Format("2006-01-02 15:04:05"), ch
		} else {
			return "", ch
		}
	}
	err := res.Decode(&s)
	if err != nil {
		return "", err
	}
	return s.Date, nil
}

func (d DocMongoRepository) Shutdown() error {
	ctx, _ := context.WithTimeout(context.Background(), 2*time.Second)
	return d.mongo.Disconnect(ctx)
}

func NewDocMongoRepository(c config.ParamsLocal) repository.DocMongoInterface {
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://" + c.MongoDb.Host + ":" + strconv.Itoa(c.MongoDb.Port) + "/"))
	if err != nil {
		log.Fatal(err)
	}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}

	return &DocMongoRepository{config: c, mongo: client}
}
