package repository

type DocMongoInterface interface {
	GetImporterSystemTime() (string, error)
	Shutdown() error
}
