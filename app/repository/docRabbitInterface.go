package repository

type DocRabbitInterface interface {
	Consumer(wakeup chan []byte)
	Producer(m []byte) error
	Shutdown() error
}
