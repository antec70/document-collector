package mysql

import (
	"document-collector/app/config"
	"document-collector/app/repository"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"log"
	"strconv"
	"time"
)

type DocMysqlRepository struct {
	config config.ParamsLocal
	mysql  *sqlx.DB
}

func (d DocMysqlRepository) Shutdown() error {
	return d.mysql.Close()
}

func (d DocMysqlRepository) GetAllFeed(sql string, date string) ([]int, error) {
	var ids []int
	er := d.mysql.Select(&ids, sql, date, d.config.Limit)

	if er != nil {
		logrus.Error(er)
		return nil, er
	}

	return ids, nil
}

func NewDocMysqlRepository(c config.ParamsLocal) repository.DocMysqlInterface {
	db, err := sqlx.Connect("mysql", c.MysqlCon.User+":"+c.MysqlCon.Password+"@("+c.MysqlCon.Host+":"+strconv.Itoa(c.MysqlCon.Port)+")/"+c.MysqlCon.Db)

	if err != nil {
		log.Fatal(err)
	}
	db.SetConnMaxIdleTime(time.Second * 8)
	db.SetMaxOpenConns(1000)
	db.SetConnMaxLifetime(time.Second * 8)

	return &DocMysqlRepository{config: c, mysql: db}
}
