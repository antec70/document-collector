package repository

type DocMysqlInterface interface {
	GetAllFeed(sql string, date string) ([]int, error)
	Shutdown() error
}
