package models

type RabbitOutput struct {
	Type string `json:"type"`
	Id   int    `json:"id"`
}

type RabbitInput struct {
	Interval string `json:"interval"`
}
