package models

type System struct {
	Id   string `bson:"_id"`
	Date string `bson:"date"`
}
